# TS-DOM
Projet Typescript pour apprendre la manipulation du DOM

## How To use
1. Cloner le projet
2. Ouvrir le dossier avec vscode
3. Faire un `npm i`
4. Faire un `npm run dev`
5. Ouvrir le navigateur sur http://localhost:5173

## Exercices

### DOM Modification ([ts](src/exo-dom.ts))
1. Mettre le texte de l'élément avec l'id para2 en bleu
2. Mettre une border en pointillet noire à la section2
3. Mettre une background color orange à l'élément de la classe colorful de la section2 
4. Mettre le h2 de la section1 en italique
5. Cacher l'élément colorful situé dans un paragraphe
6. Changer le texte de para2 pour "modified by JS"
7. Changer le href du lien de la section1 pour le faire aller sur simplonline.co (il faudra le sélectionner en tant que HTMLAnchorElement)
8. Rajouter la classe big-text sur le h2 de la section2
9. Bonus : Faire que tous les paragraphe du document soit en italique

### Exo Counter ([ts](src/exo-counter.ts))
1. Créer les fichiers exo-counter.html / .ts
2. Dans le le html, rajouter 2 button, un + et un -
3. Dans le ts, créer une variable counter initialisée à zéro
4. Capturer les 2 button (2 variables et 2 querySelector) puis avec un addEventListener faire que quand on click sur l'un ça incrémente le counter et ça affiche sa valeur en console
5. Faire que quand on click sur l'autre, ça fasse pareil, mais en décrémentant
6. Rajouter un élément span, ou div ou autre dans le html et lui mettre 0 dedans
7. Modifier nos eventListener pour faire qu'à la place ou en plus du console log, ça  aille modifier le innerHTML/textContent de l'élément en question pour lui mettre la valeur actuelle du counter

**Bonus :** faire qu'au double click sur la valeur du compteur, ça repasse celui ci à zéro


### Amazing moving square ([ts](src/exo-square.ts))
1. Créer un nouveau fichier html/ts pour exo-square
2. Dans le html, créer une div#playground et lui mettre en css  une height à 100vh, et une position relative. Dans cette div, rajouter une autre div#square et faire en sorte via le css qu'elle ressemble à un carré rouge (ou autre chose, on s'en fout un peu) et qu'elle soit en position absolute
3. Dans le TS, capturer le playground et le square puis rajouter un addEventListener au click sur le playground
4. Dans ce listener, rajouter un argument event à la fat-arrow, regarder via le console log si le event ne contiendrait pas des valeurs indiquant où on a cliqué (spoiler : c'est le cas)
5. Utiliser ces valeurs pour les assigner en top et en left de notre square (il va falloir concaténé 'px' aux valeurs en question)
6. On peut rajouter une petite transition à notre css sur le carré pour qu'il glisse tel un cygne sur un lac gelé

**Bonus :** Faire un autre carré qui suit la souris dès qu'elle bouge.

**Bonus du bonus :** faire qu'il suive la souris seulement quand on a le click appuyé dessus (un drag'n drop quoi)


### Form Calcul ([ts](src/exo-calcul.ts))
1. Dans le fichier HTML, rajouter un form qui contiendra 2 input type number ainsi qu'un span qui contiendra le résultat du calcul, ainsi qu'un button qui servira à lancer le calcul (c'est les étapes d'après)
2. Côté typescript, faire en sorte de rajouter un event listener sur le submit du formulaire qui va récupérer la propriété value de chaque input, et les afficher en console (il faut donc querySelector le form et les deux input)
3. Pour que le formulaire ne recharge pas la page, il va falloir rajouter l'argument event et dans la fonction commencer par déclencher un event.preventDefault()
4. Une fois qu'on a les valeurs dans la console, faire en sorte de les additionner et afficher le résultat en console
5. Quand ça marche, faire en sorte d'assigner ce résultat au span
6. Rajouter dans le html un select qui aura comme options + - / et *, modifier l'eventListener pour faire qu'on récupère la value de ce select et selon l'opérateur on fait un calcul ou un autre

### Exo Création d'élément ([ts](src/exo-create.ts))
1. Créer un fichier exo-create.html et .ts
2. Dans le HTML, mettre un élément, par exemple un main et le querySelector depuis le JS, mettre un button dans le main et le capturer aussi
3. Rajouter un event au click sur le bouton qui va faire un createElement d'un paragraphe avec 'coucou' dedans et qui va l'append dans le main

**Bonus** Rajouter un boucle pour que ça crée 4 élément à la fois pour chaque click