const form = document.querySelector('form');
const aInput = document.querySelector<HTMLInputElement>('#a');
const bInput = document.querySelector<HTMLInputElement>('#b');
const operatorSelect = document.querySelector<HTMLSelectElement>('#operator');
const spanResult = document.querySelector<HTMLElement>('#result');

form.addEventListener('submit', (event) => {
    event.preventDefault();

    let operator = operatorSelect.value;

    if(operator == '-') {

        const result = aInput.valueAsNumber - bInput.valueAsNumber;
        spanResult.textContent = result.toString();
    }

    if(operator == '*') {

        const result = aInput.valueAsNumber * bInput.valueAsNumber;
        spanResult.textContent = result.toString();
    }

    if(operator == '/') {

        const result = aInput.valueAsNumber / bInput.valueAsNumber;
        spanResult.textContent = result.toString();
    }
    
});

