import './style.css'; //On peut charger son css depuis le ts ou depuis le html

const playground = document.querySelector<HTMLElement>('#playground');
const square = document.querySelector<HTMLElement>('.square');

/**
 * L'argument event (peu importe son nom en fait, mais la plupart du temps il est appelé
 * event ou e) dans la fonction d'un addEventListener permettra d'accéder à des
 * informations sur l'élément qui vient d'être déclenché, comme la position de la souris
 * quel click est enfoncé, quelle touche est enfoncée etc.
 * Ici, on utilise la position de la souris et quel bouton de la souris est enfoncé
 * pour faire que le carré "suive" la souris seulement si on maintient le clic gauche
 */
playground.addEventListener('mousemove', (event) => {
    if(event.buttons == 1) {

        square.style.top = event.clientY+'px';
        square.style.left = event.clientX+'px';
    }
});