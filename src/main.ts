//On capture un élément HTML avec querySelector et on précise le type d'élément sélectionné avec <HTMLElement>
//(on fait ça pasque typescript n'est pas sûr de ce qu'on récupère comme type d'élément vu qu'on utilise un id)
const app = document.querySelector<HTMLElement>('#app');

//On modifie le textContent de notre élément depuis le script ce qui impactera l'affichage
app.textContent = 'autre chose';

//On affiche en console la valeur actuelle du textContent, ça marche avec n'importe quel autre propriété
console.log(app.textContent);

/**
 * On peut rajouter un event listener sur n'importe quel élément capturé. Le addEventListener
 * attend en premier argument l'événement à surveiller et en deuxième argument une fonction
 * (ici fonction anonyme en fat-arrow) qui sera déclenchée au moment où l'événement en question
 * sera déclenché sur l'élément ciblé.
 * On peut également rajouter un argument event à la fonction qui contiendra tout un tas
 * d'informations sur l'événement déclenché
 */
app.addEventListener('click', () => {
  console.log('clicked');
});

/**
 * On peut créer un élément HTML depuis le JS avec la fonction
 * createElement("type d'élément à créer")
 */
const section = document.createElement('section');
//On se retrouve alors avec une variable qui contient exactement la même
//chose que si on avait fait un querySelector et qu'on peut donc manipuler à l'envie
section.classList.add('ma-classe');
section.textContent = 'créée par le TS';
//Pour que l'élément créé s'affiche, il faudra l'append dans un élément existant déjà dans le DOM
app.append(section);