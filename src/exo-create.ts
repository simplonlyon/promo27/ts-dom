const main = document.querySelector('main');
const button = document.querySelector('button');

button.addEventListener('click', () => {
    
    
    const paragraph = document.createElement('p');
    paragraph.textContent = 'Coucou';
    main.append(paragraph);
    
});

const ul = document.querySelector('ul');
const btnAdd = document.querySelector<HTMLElement>('#btn-add');
const input = document.querySelector('input');
const p = document.querySelector('p');
const promo:string[] = ['Djino', 'Aymeric', 'Chieko', 'Matthieu', 'Souad'];

displayPromo();

/**
 * Au click sur le bouton, on note qu'on ne crée pas directement un nouveau li, à la
 * place on push une nouvelle valeur dans le tableau et on relance la fonction d'affichage
 * qui se base sur le tableau pour générer les balises
 */
btnAdd.addEventListener('click', () => {
    promo.push(input.value);

    console.log(promo);
    displayPromo();
});

/**
 * Fonction qui va boucler sur le tableau et créer un élément HTML pour chaque item
 * présent dans celui ci. Il faudra la relancer à chaque fois que les données du tableau
 * changens
 */
function displayPromo() {
    ul.innerHTML = '';
    for (const student of promo) {
        const li = document.createElement('li');
        li.textContent = student;
        li.addEventListener('click', () => {
            p.textContent += student+' ';
        })
        ul.append(li);

    }
}
