import './modal.css';

const modal = document.querySelector<HTMLElement>('aside');
const imgModal = modal.querySelector('img');
const btnClose = document.querySelector<HTMLElement>('#close');
const sectImages = document.querySelector<HTMLElement>('#images')

const pictures = ['https://cdn.pixabay.com/photo/2021/07/05/14/07/dog-6389277_1280.jpg', 'https://cdn.pixabay.com/photo/2019/08/19/07/45/corgi-4415649_1280.jpg', 'https://cdn.pixabay.com/photo/2021/07/07/14/40/dog-6394502_1280.jpg', 'https://cdn.pixabay.com/photo/2016/08/09/16/59/welsh-corgi-1581119_1280.jpg']
let selected: string|null = null;

updateDisplay();


btnClose.addEventListener('click', () => {
    selected = null;
    updateDisplay();
});


function updateDisplay() {
    
    if(selected) {
        modal.classList.remove('hide');
        imgModal.src = selected;
    } else {
        modal.classList.add('hide');
    }
    sectImages.innerHTML = '';
    for(const link of pictures) {
        const img = document.createElement('img');
        img.src = link;
        img.width = 100;
        img.addEventListener('click', () =>{
            selected = link;
            console.log(selected);
            updateDisplay();
        });
        sectImages.append(img);
    }
}
