let counter = 0;

const btnPlus = document.querySelector<HTMLElement>('#plus');
const btnMinus = document.querySelector<HTMLElement>('#minus');
const spanCounter = document.querySelector<HTMLElement>('#counter');


btnPlus.addEventListener('click', () => {
    counter++;
    updateDisplay();
});

btnMinus.addEventListener('click', () => {
    counter--;
    updateDisplay();
});

spanCounter.addEventListener('dblclick', () => {
    counter = 0;
    updateDisplay();
});

function updateDisplay() {
    console.log(counter);
    spanCounter.textContent = counter.toString();
}
