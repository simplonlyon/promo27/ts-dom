const para2 = document.querySelector<HTMLElement>('#para2');
const section2 = document.querySelector<HTMLElement>('#section2');
const colorfulSect2 = document.querySelector<HTMLElement>('#section2 .colorful');
const h2Sect1 = document.querySelector<HTMLElement>('#section1 h2');
const colorfulPara = document.querySelector<HTMLElement>('p .colorful');
const anchorSect1 = document.querySelector<HTMLAnchorElement>('#section1 a');
const h2Sect2 = document.querySelector<HTMLElement>('#section2 h2');
const paras = document.querySelectorAll('p');

para2.style.color = 'blue';

section2.style.border = 'dashed';

colorfulSect2.style.backgroundColor = 'orange';

h2Sect1.style.fontStyle = 'italic';

colorfulPara.style.display = 'none';

para2.innerHTML = 'modified by JS';

anchorSect1.href = 'https://simplonline.co';

h2Sect2.classList.add('big-text');

//On boucle sur le résultat du querySelectorAll pour appliquer la modification à chaque élément sélectionnés
for(const para of paras) {
    para.style.fontStyle = 'italic';
}